import regex as re
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from components.engine import Engine
from components.services import RecordService
from components.start import start
from config import BOT_TOKEN


def received_information(bot, update, user_data):
    text = update.message.text
    category = ''
    show_category = False
    Engine.init(bot, update)
    engine = Engine()

    # If someone appeals to bot
    if re.match(r'(?i)(.*?)(журн\S*)(.*)', text):
        # get actual query string
        text = re.match(r'(?i)(.*?)(журн\S*)(.*)', text).groups()[2].strip()

        # get category if exists
        if re.match(r'(?i)(\p{Cyrillic}\d+|\S+)\s*(.*)', text):
            marker = re.match(r'(?i)(\p{Cyrillic}\d+|\S+)\s*(.*)', text).groups()[0]
            if marker[0] in ['+', '?', '!', '*', '=', '-']:
                pass
            else:
                category, text = re.match(r'(?i)(\p{Cyrillic}\d+|\S+)\s*(.*)', text).groups()
                show_category = True

        # add record or cat
        if re.match(r'(?i)(^\+)\s?(.*)\s?', text):
            record = re.match(r'(?i)(^\+)(.*)\s?', text).groups()[1].strip()
            if category.upper() == "КВЕСТ":
                record += " | Додав {0}.".format(update.message.from_user.first_name)
            record = RecordService.add(record, category)
            # add author signature
           
            if category:
                engine.send(
                "Запис <b>№{0}</b> додано в категорію <b>{1}</b>.".format(
                    record.record_id, record.category))
            else:
                engine.send("Запис <b>№{0}</b> додано.".format(record.record_id))
        # delete record or cat
        elif re.match(r'(?i)(^\-)', text):
            if category and re.match(r'(?i)(^\-$)', text):
                result = RecordService.delete_category(category)
                if result:
                    engine.send("Видаленo всі записи з категорією <b>{0}</b>".format(category))
                else:
                    engine.send("Не знайдено записів з категорією <b>{0}</b>".format(category))
            else:
                id = re.match(r'(?i)(^\-)\s?(\d+)', text).groups()[1].strip()
                RecordService.remove(id)
                engine.send("Запис <b>№{0}</b> видалено.".format(id))
        # edit record
        elif re.match(r'(?i)(^\=)\s?(\d+)\s?(.*)\s?', text):
            if re.match(r'(?i)(^\=)\s?(\d+)\s?(\#)\s?(\S+)\s?', text):
                records_raw = re.match(r'(?i)(^\=)\s?(\d+)\s?(\#)\s?(\S+)\s?', text).groups()
                record = RecordService.edit_category(records_raw[1],records_raw[3])
                engine.send("Запис <b>№{0}</b> перенесено до категорії {1}".format(record.record_id, record.category))
            else:
                record_raw = re.match(r'(?i)(^\=)\s?(\d+)\s?(.*)\s?', text).groups()
                record_id = record_raw[1]
                record = record_raw[2]
                # add editor signature
                saved_record = RecordService.show_one_by_id(record_id)
                if saved_record.category.upper() == "КВЕСТ":
                    record += " | Змінив {0}.".format(update.message.from_user.first_name)
                
                record = RecordService.edit(record_id, record)
                engine.send("Запис <b>№{0}</b> оновлено.".format(record.record_id))
        # edit category
        elif re.match(r'(?i)(\={1})\s*(\S+)', text):
            new_cat = re.match(r'(?i)(\={1})\s*(\S+)', text).groups()[1]
            RecordService.change_category(category, new_cat)
            engine.send("Категорія <b>{0}</b> тепер має назву <b>{1}</b>.".format(category, new_cat))
        # mark record as completed
        elif re.match(r'(?i)(^\!)\s?(\d+)', text):
            id = re.match(r'(?i)(^\!)\s?(\d+)\s?(.*)\s?', text).groups()[1].strip()
            record = RecordService.complete(id)
            engine.send("Запис <b>№{0}</b> позначено завершеним.".format(record.record_id))
        #get record by id
        elif re.match(r'(?i)(^[0-9]+)', text) or re.match(r'(?i)(^[0-9]+)', category):
            try:
                id = int(re.match(r'(?i)(^\d*)', text).groups()[0])
            except ValueError:
                id = int(re.match(r'(?i)(^\d*)', category).groups()[0])
            record = RecordService.show_one_by_id(id)
            if record:
                text = engine.convert_br_into_breaks_if_exist(record.text)
                engine.send("<b>{0}.</b> {1}".format(record.record_id, text))
            else:
                engine.send("Жодних записів за цим запитом.")
        #show block
        elif re.match(r'(?i)(^\*)', text):
            try:
                # show all active
                if re.match(r'(?i)(^\*\s?\+)', text):
                    # show active in range
                    if re.match(r'(?i)(^\*\s?\+)\s?(\d*)\:(\d*)', text):
                        raw_groups = re.match(r'(?i)(^\*\s?\+)\s?(\d*)\:(\d*)', text).groups()
                        start_id = raw_groups[1]
                        stop_id = raw_groups[2]
                        if start_id and stop_id:
                            records = RecordService.show_active_in_range(
                                int(start_id), int(stop_id), category)
                        elif start_id:
                            records = RecordService.show_last_n_active(
                                int(start_id), category)
                        elif stop_id:
                            records = RecordService.show_first_n_active(
                                int(stop_id), category)

                        message = engine.prepare_records_list_message(records, show_category=show_category)
                        engine.send(message)
                    else:
                        records = RecordService.show_active(category)
                        message = engine.prepare_records_list_message(records, show_category=show_category)
                        engine.send(message)
                # show all completed
                elif re.match(r'(?i)(^\*\s?\!)', text):
                    # show completed in range
                    if re.match(r'(?i)(^\*\s?\!)\s?(\d*)\:(\d*)', text):
                        raw_groups = re.match(r'(?i)(^\*\s?\!)\s?(\d*)\:(\d*)', text).groups()
                        start_id = raw_groups[1]
                        stop_id = raw_groups[2]
                        if start_id and stop_id:
                            records = RecordService.show_inactive_in_range(
                                int(start_id), int(stop_id), category)
                        elif start_id:
                            records = RecordService.show_last_n_inactive(
                                int(start_id), category)
                        elif stop_id:
                            records = RecordService.show_first_n_inactive(
                                int(stop_id), category)
                        message = engine.prepare_records_list_message(records, show_category=show_category)
                        engine.send(message)
                    else:
                        records = RecordService.show_inactive(category)
                        message = engine.prepare_records_list_message(records, show_category=show_category)
                        engine.send(message)
                # show all in range
                elif re.match(r'(?i)(^\*)\s?(\d*)\:(\d*)', text):
                    raw_groups = re.match(r'(?i)(^\*)\s?(\d*)\:(\d*)', text).groups()
                    start_id = raw_groups[1]
                    stop_id = raw_groups[2]
                    if start_id and stop_id:
                        records = RecordService.show_in_range(
                            int(start_id), int(stop_id), category)
                    elif start_id:
                        records = RecordService.show_last_n(
                            int(start_id), category)
                    elif stop_id:
                        records = RecordService.show_first_n(
                            int(stop_id), category)
                    message = engine.prepare_records_list_message(records, mark_completed=True, show_category=show_category)
                    engine.send(message)
                elif re.match(r'(?i)(^\*\s?\#$)', text):
                    categories = RecordService.get_all_categories()
                    message = engine.prepare_categories_list_message(categories)
                    engine.send(message)
                # else show all
                else:
                    records = RecordService.show_all(category)
                    message = engine.prepare_records_list_message(records, mark_completed=True, show_category=show_category)
                    engine.send(message)
            except IndexError:
                engine.send("Жодних записів за цим запитом.")
        
        # find block
        elif re.match(r'(?i)(^\?)(.*)\s?', text):
            # find active with search string
            if re.match(r'(?i)(^\?\s?\+)\s?(\S*)\s?(.*)\s?', text):
                keyword = re.match(r'(?i)(^\?\s?\+)\s?(\S*)\s?(.*)\s?', text).groups()[1].strip()
                records = RecordService.find_active_by_keyword(keyword, category)
                message = engine.prepare_records_list_message(
                    records, show_category=show_category, keyword=keyword)
                engine.send(message)
            # find completed with search string
            elif re.match(r'(?i)(^\?\s?\!)\s?(\S*)\s?(.*)\s?', text):
                keyword = re.match(r'(?i)(^\?\s?\!)\s?(\S*)\s?(.*)\s?', text).groups()[1].strip()
                records = RecordService.find_inactive_by_keyword(keyword, category)
                message = engine.prepare_records_list_message(
                    records, show_category=show_category, keyword=keyword)
                engine.send(message)
            # find all with keyword
            else:
                keyword = re.match(r'(?i)(^\?)\s?(\S*)\s?(.*)\s?', text).groups()[1].strip()
                records = RecordService.find_all_by_keyword(keyword, category)
                message = engine.prepare_records_list_message(
                    records, mark_completed=True, show_category=show_category, keyword=keyword)
                engine.send(message)
        # if help
        elif category.upper() in ['ХЕЛП','HELP','ПОМОЩЬ', 'ДОПОМОГА', 'ІНСТРУКЦІЯ','ИНСТРУКЦИЯ']:
            start(bot, update)

def request_handler():
    # bot token
    updater = Updater(BOT_TOKEN)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(MessageHandler(
        Filters.text, received_information, pass_user_data=True))

    updater.start_polling()
    updater.idle()
