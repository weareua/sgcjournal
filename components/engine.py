import telegram


class Engine():
    """
    Creates engine object and proceed essential methods
    """
    inited = False

    @staticmethod
    def init(bot, update):
        """
        Init static members for entire class
        """
        Engine.bot = bot
        Engine.chat_id = update.message.chat_id
        Engine.sender_name = update.message.from_user.first_name
        Engine.inited = True

        Engine.bot.setWebhook()
    
    def prepare_records_list_message(
        self, records, mark_completed=False, show_category=False, keyword=''):
        message = ''
        # write title if we show all records in single category
        try:
            if show_category:
                message += "<b>{0}</b> \n\n".format(records[0].category)
            for record in records:
                text = record.text
                # add category marker for general records output
                if not show_category and len(record.category):
                    message += "<b>{0}:</b> ".format(record.category)
                if mark_completed:
                    if record.active is False:
                        message += "<i>[Виконано]</i> "
                # highlight search keyword
                if len(keyword):
                    text = text.replace(keyword, "<b>{0}</b>".format(keyword))
                # convert <br> into line breaks
                text = self.convert_br_into_breaks_if_exist(text)
                
                message += '<b>{0}.</b> {1} \n\n'.format(record.record_id, text)
                
                
        except IndexError:
            message = "Жодних записів за цим запитом."
        return message

    def convert_br_into_breaks_if_exist(self, text):
        if "br" in text:
            text = text.replace("<br>", "\n")
        return text

    def prepare_categories_list_message(self, categories):
        message = ''
        for cat in categories:
            message += '<b>{0}</b> \n'.format(cat)
        return message

    def send(self, message):
        """
        Send proeeded message
        """
        message_list = []
        if message is '':
            message = 'Жодних записів за цим запитом.'
        # handle big messages:
        while len(message) > 4095:
            message_list.append(message[0:4095])
            message = message[4095:]
        message_list.append(message)
        message_list.reverse()

        while len(message_list) > 0:
            self.bot.sendMessage(
                chat_id=self.chat_id, text=message_list.pop(), parse_mode=telegram.ParseMode.HTML)
