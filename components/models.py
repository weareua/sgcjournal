import os
from pony.orm import *


db = Database()


class Record(db.Entity):
    record_id = Required(int)
    text = Required(str)
    category = Optional(str)
    active = Required(bool, default=True, sql_default='1')


# bind orm with db
db.bind(provider='sqlite', filename=os.path.join(
    os.path.dirname( __file__ ), '..', 'journal.sqlite'), create_db=True)
# create db tables if not extst
db.generate_mapping(create_tables=True)
set_sql_debug(True)