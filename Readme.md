The bot purpose is to create and store notes.
Features:

+ <record: string> : add record
- <record id: id> : delete record
= <record id: id> : edit record
! <record id: id> : mark record as completed
<record id> : show record with concrete id

* : show all
*+ : show all active
*! : show all inactive

* <record id>:<record id> : show set of records in defined scope
*+ <record id>:<record id> : show set of active records in defined scope
*! <record id>:<record id> : show set of inactive records in defined scope

*:<n> : show first n records
*+:<n> : show first n active records
*!:<n> : show first n inactive records

*<n>: : show last n records (desc sort)
*+<n>: : show last n active records (desc sort)
*!<n>: : show last n inactive records (desc sort)

? <word> : find all records with this word
?+ <word> : find aсtive records with this word
?! <word> : find inactive records with this word